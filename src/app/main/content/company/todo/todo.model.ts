import { PatronModel } from "../../_models/patron/patron.model";

export class Todo
{
    id: number;
    title: PatronModel;
    notes: string;
    startDate: string;
    starred: boolean;
    important: boolean;
    deleted: boolean;
    queueNumber: number;
    associateId: number;
    locationId: number;
    scheduleId: number;

    constructor(todo)
    {
        {
            this.id = todo.id;
            this.title = todo.title;
            this.notes = todo.notes;
            this.startDate = todo.startDate;
            this.starred = todo.starred;
            this.important = todo.important;
            this.deleted = todo.deleted;
            this.queueNumber = todo.queueNumber;
            this.associateId = todo.associateId;
            this.locationId = todo.locationId;
            this.scheduleId = todo.scheduleId;
        }
    }

    toggleStar()
    {
        this.starred = !this.starred && this.important;
    }

    toggleImportant()
    {
        this.important = !this.important;
        if(!this.important)
        {
            this.starred = false;
        }
    }

    toggleDeleted()
    {
        this.deleted = !this.deleted;
    }
}
