import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import {  Router,ActivatedRoute } from '@angular/router';

import { Subscription, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, map, tap } from 'rxjs/operators';

import { FuseUtils } from '@fuse/utils';
import { fuseAnimations } from '@fuse/animations';

import { Todo } from '../todo.model';
import { TodoService } from '../todo.service';

import { PatronModel } from '../../../_models/patron/patron.model';
import { PatronService } from '../../../_services/patron.service';
import { CompanyService } from '../../../_services/company.service';
import { ScheduleService } from '../../../_services/schedule.service';
import { LocationModel } from '../../../_models/associate/locations.model';
import { AssociateModel } from '../../../_models/associate/associate.model';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ScheduleModel } from '../../../_models/associate/schedule.model';
import { getDay } from 'date-fns';

@Component({
    selector   : 'fuse-todo-details',
    templateUrl: './todo-details.component.html',
    styleUrls  : ['./todo-details.component.scss'],
    animations : fuseAnimations
})
export class FuseTodoDetailsComponent implements OnInit, OnDestroy
{
    todo: Todo;
    tags: any[];
    formType: string;
    todoForm: FormGroup;
    events: Date[] = [];
    newApptLocation : number = 0;
    newApptAssociate : number = 0;
    @ViewChild('titleInput') titleInputField;

    onFormChange: any;
    onCurrentTodoChanged: Subscription;
    onTagsChanged: Subscription;
    onNewTodoClicked: Subscription;
    options : any;
    filteredOptions: PatronModel[];
    locations: LocationModel[];
    associates: AssociateModel[];
    availableSchedules: ScheduleModel[];
    allSchedules: ScheduleModel[];
    companyId: number;

    constructor(
        private todoService: TodoService,
        private formBuilder: FormBuilder,
        private patronService: PatronService,
        private activatedRoute: ActivatedRoute, 
        private companyService: CompanyService, 
        private scheduleService: ScheduleService,
        private router: Router
    )
    { }

    ngOnInit()
    {
        this.companyId = this.activatedRoute.snapshot.params.companyId;
        this.patronService.getPatrons().subscribe((patrons)=> {
            this.options = patrons;
        });

        this.locations = this.companyService.getLocations(this.companyId);
        // Subscribe to update the current todoT
        this.onCurrentTodoChanged =
            this.todoService.onCurrentTodoChanged
                .subscribe(([todo, formType]) => {
                    if ( todo && formType === 'edit' )
                    {
                        this.formType = 'edit';
                        this.todo = todo;
                        this.todoForm = this.createTodoForm();
                        this.onFormChange =
                            this.todoForm.valueChanges.pipe(
                                debounceTime(500),
                                distinctUntilChanged()
                            ).subscribe(data => {
                                this.todoService.saveTodo(data);
                            });
                    }
                });

        // Subscribe to update on tag change
        this.onTagsChanged =
            this.todoService.onTagsChanged
                .subscribe(labels => {
                    this.tags = labels;
                });

        // Subscribe to update on tag change
        this.onNewTodoClicked =
            this.todoService.onNewTodoClicked
                .subscribe(() => {
                    this.todo = new Todo({});
                    this.todo.id = 0;
                    this.formType = 'new';
                    this.todoForm = this.createTodoForm();
                    this.focusTitleField();
        
                   this.todoForm.controls.title.valueChanges.pipe(
                        startWith(''),
                        map(val => typeof(val) === "string" && val ? this.filter(val) : this.options)
                      ).subscribe(result => {
                          this.filteredOptions = result;
                      });
                    this.todoService.onCurrentTodoChanged.next([this.todo, 'new']);
                });
    }

    
  filter(val: string): PatronModel[] {
    return this.options.filter(option =>
      option.patronName.toLowerCase().includes(val.toLowerCase()) ||
      option.email.toLowerCase().includes(val.toLowerCase()) ||
      option.phoneNumber.includes(val)
    );
  }

  displayFn(patron?: PatronModel): string | undefined {
    return patron ? patron.patronName : undefined;
  }

  populateSchedules(event: MatDatepickerInputEvent<Date>) {
      var apptDate = new Intl.DateTimeFormat('en-US').format(new Date(event.value)); 
      var dayOfWeek = getDay(apptDate);
      this.availableSchedules = this.allSchedules.filter(x=>x.dayOfWeek === dayOfWeek);
  }

  onLocationChange(locationId: number){
      this.associates = this.locations.filter(x=>x.locationId === locationId)[0].associates;
      this.newApptLocation = locationId;
  }

  onAssociateChange(associateId: number){
    this.newApptAssociate = associateId;
    this.scheduleService.getSchedules(this.newApptLocation,this.newApptAssociate).subscribe(result =>{
        this.allSchedules = result;
    });
}
    ngOnDestroy()
    {
        if ( this.onFormChange )
        {
            this.onFormChange.unsubscribe();
        }

        this.onCurrentTodoChanged.unsubscribe();
        this.onNewTodoClicked.unsubscribe();
    }

    focusTitleField()
    {
        setTimeout(() => {
            this.titleInputField.nativeElement.focus();
        });
    }

    createTodoForm()
    {
        if(this.todo.locationId > 0)
         this.associates = this.locations.filter(x=>x.locationId === this.todo.locationId)[0].associates;
        return this.formBuilder.group({
            'id'       : [this.todo.id],
            'title'    : [this.todo.title],
            'notes'    : [this.todo.notes],
            'startDate': [this.todo.startDate],
            'starred'  : [this.todo.starred],
            'important': [this.todo.important],
            'deleted'  : [this.todo.deleted],
            'queueNumber' : [this.todo.queueNumber],
            'associateId' : [this.todo.associateId],
            'locationId'  : [this.todo.locationId],
            'scheduleId':  [this.todo.scheduleId]
        });
    }

    /**
     * Toggle Deleted
     * @param event
     */
    toggleDeleted(event)
    {
        event.stopPropagation();
        this.todo.toggleDeleted();
        this.todoService.deleteTodo(this.todo);
    }

    addTodo()
    {
        var newTodo = this.todoForm.getRawValue();
        newTodo.startDate = new Intl.DateTimeFormat('en-US').format(new Date(newTodo.startDate));
        newTodo.queueNumber = newTodo.queueNumber || 0;
        newTodo.deleted = newTodo.queueNumber || false;
        newTodo.important = newTodo.queueNumber || false;
        newTodo.starred = newTodo.starred || false;
        this.todoService.saveTodo(newTodo);
        this.router.navigate(['/company/'+ this.companyId +'/appointments/added'])
    }
}
