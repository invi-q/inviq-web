import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { TodoService } from '../../todo.service';
import { CompanyService } from '../../../../_services/company.service'
import { LocationModel } from '../../../../_models/associate/locations.model';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
    selector   : 'fuse-todo-main-sidenav',
    templateUrl: './main-sidenav.component.html',
    styleUrls  : ['./main-sidenav.component.scss'],
    animations : fuseAnimations
})
export class FuseTodoMainSidenavComponent implements OnInit, OnDestroy
{
    folders: any[];
    filters: any[];
    tags: any[];
    accounts: LocationModel[];
    selectedAccount: LocationModel;
    companyId: number;

    onFiltersChanged: Subscription;
    onTagsChanged: Subscription;
    onSelectedDateChanged: Subscription;
    appointmentsDate: string;

    constructor(private todoService: TodoService, private router: Router, private companyService: CompanyService, private activatedRoute: ActivatedRoute)
    {     
    }

    ngOnInit()
    {
        this.activatedRoute.params.subscribe(result=> {
            this.companyId = result.companyId;
            this.appointmentsDate = result.selectedDate;
          });
        this.onFiltersChanged =
            this.todoService.onFiltersChanged
                .subscribe(filters => {
                    this.filters = filters;
                });

        this.onTagsChanged =
            this.todoService.onTagsChanged
                .subscribe(tags => {
                    this.tags = tags;
                });
    }

    ngOnDestroy()
    {
        this.onFiltersChanged.unsubscribe();
        this.onTagsChanged.unsubscribe();
    }

    newTodo()
    {
        this.router.navigate(['/company/'+ this.companyId +'/appointments/']).then(() => {
            setTimeout(() => {
                this.todoService.onNewTodoClicked.next('');
            });        
        });
    }

    updateAppointments(event: MatDatepickerInputEvent<Date>)
    {
      var apptDate = (new Intl.DateTimeFormat('en-US').format(new Date(event.value))).replace(/\//g, "-");  
      this.appointmentsDate = apptDate;
      this.router.navigate(['/company/'+ this.companyId +'/appointments/date/'+ apptDate]);      
    }
}
