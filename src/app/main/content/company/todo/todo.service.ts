import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AuthService } from "../../_services/auth.service";

import { FuseUtils } from '@fuse/utils';

import { Todo } from './todo.model';
import { environment } from 'environments/environment';

@Injectable()
export class TodoService implements Resolve<any>
{
    todos: Todo[];
    selectedTodos: Todo[];
    currentTodo: Todo;
    searchText = '';
    httpOptions: any;

    filters: any[];
    tags: any[];
    routeParams: any;
    selectedDate = '';

    onTodosChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSelectedTodosChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onCurrentTodoChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    onFiltersChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onTagsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSearchTextChanged: BehaviorSubject<any> = new BehaviorSubject('');
    onNewTodoClicked: Subject<any> = new Subject();

    constructor(
        private http: HttpClient,
        private location: Location,
        private authService: AuthService
    )
    {
        this.selectedTodos = [];
        if(this.authService.isAuthenticated())
        {
          this.httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              "Authorization": "Bearer "+ this.authService.getToken()
            })
          };
        }
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;
        this.selectedDate = this.routeParams.selectedDate;
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getFilters(),
                this.getTags(),
                this.getTodos()
            ]).then(
                () => {
                    if ( this.routeParams.todoId )
                    {
                        this.setCurrentTodo(this.routeParams.todoId);
                    }
                    else
                    {
                        this.setCurrentTodo(null);
                    }

                    this.onSearchTextChanged.subscribe(searchText => {
                        if ( searchText !== '' )
                        {
                            this.searchText = searchText;
                            this.getTodos();
                        }
                        else
                        {
                            this.searchText = searchText;
                            this.getTodos();
                        }
                        
                    });
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all filters
     * @returns {Promise<any>}
     */
    getFilters(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(environment.API_URL + "/company/"+this.routeParams.companyId+"/locations", this.httpOptions)
                .subscribe((response: any) => {
                    this.filters = JSON.parse(response);
                    this.onFiltersChanged.next(this.filters);
                    resolve(this.filters);
                }, reject);
        });
    }

    /**
     * Get all tags
     * @returns {Promise<any>}
     */
    getTags(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(environment.API_URL + "/company/"+this.routeParams.companyId+"/associates", this.httpOptions)
                .subscribe((response: any) => {
                    this.tags = JSON.parse(response);
                    this.onTagsChanged.next(this.tags);
                    resolve(this.tags);
                }, reject);
        });
    }

    /**
     * Get todos
     * @returns {Promise<Todo[]>}
     */
    getTodos(): Promise<Todo[]>
    {
        if ( this.routeParams.tagHandle )
        {
            return this.getTodosByTag(this.routeParams.tagHandle);
        }

        if ( this.routeParams.filterHandle )
        {
            return this.getTodosByFilter(this.routeParams.filterHandle);
        }

        return this.getTodosByParams(this.routeParams);
    }

    /**
     * Get todos by params
     * @param handle
     * @returns {Promise<Todo[]>}
     */
    getTodosByParams(handle): Promise<Todo[]>
    {
        if(!this.selectedDate)
        {
            this.selectedDate = '';
        }
        return new Promise((resolve, reject) => {
            this.http.get(environment.API_URL + "/appointments/company/"+this.routeParams.companyId+ "?selectedDate="+this.selectedDate, this.httpOptions)
                .subscribe((todos: any) => {
                    this.todos = todos.map(todo => {
                        return new Todo(todo);
                    });

                    this.todos = FuseUtils.filterArrayByString(this.todos, this.searchText);

                    this.onTodosChanged.next(this.todos);
                    resolve(this.todos);
                });
        });
    }

    /**
     * Get todos by filter
     * @param handle
     * @returns {Promise<Todo[]>}
     */
    getTodosByFilter(handle): Promise<Todo[]>
    {
        if(!this.selectedDate)
        {
            this.selectedDate = '';
        }

        return new Promise((resolve, reject) => {

            this.http.get(environment.API_URL + "/appointments/location/"+handle + "?selectedDate="+this.selectedDate, this.httpOptions)
                .subscribe((todos: any) => {
                    this.todos = todos.map(todo => {
                        return new Todo(todo);
                    });

                    this.todos = FuseUtils.filterArrayByString(this.todos, this.searchText);
                    this.onTodosChanged.next(this.todos);

                    resolve(this.todos);

                }, reject);
        });
    }

    /**
     * Get todos by tag
     * @param handle
     * @returns {Promise<Todo[]>}
     */
    getTodosByTag(handle): Promise<Todo[]> {
        if(!this.selectedDate)
        {
            this.selectedDate = '';
        }
        return new Promise((resolve, reject) => {
            this.http.get(environment.API_URL + "/appointments/company/" + this.routeParams.companyId + "/associate/" + handle + "?selectedDate="+this.selectedDate, this.httpOptions)
                .subscribe((todos: any) => {

                    this.todos = todos.map(todo => {
                        return new Todo(todo);
                    });

                    this.todos = FuseUtils.filterArrayByString(this.todos, this.searchText);

                    this.onTodosChanged.next(this.todos);

                    resolve(this.todos);

                }, reject);
        });
    }

    /**
     * Toggle selected todo by id
     * @param id
     */
    toggleSelectedTodo(id)
    {
        // First, check if we already have that todo as selected...
        if ( this.selectedTodos.length > 0 )
        {
            for ( const todo of this.selectedTodos )
            {
                // ...delete the selected todo
                if ( todo.id === id )
                {
                    const index = this.selectedTodos.indexOf(todo);

                    if ( index !== -1 )
                    {
                        this.selectedTodos.splice(index, 1);

                        // Trigger the next event
                        this.onSelectedTodosChanged.next(this.selectedTodos);

                        // Return
                        return;
                    }
                }
            }
        }

        // If we don't have it, push as selected
        this.selectedTodos.push(
            this.todos.find(todo => {
                return todo.id === id;
            })
        );

        // Trigger the next event
        this.onSelectedTodosChanged.next(this.selectedTodos);
    }

    /**
     * Toggle select all
     */
    toggleSelectAll()
    {
        if ( this.selectedTodos.length > 0 )
        {
            this.deselectTodos();
        }
        else
        {
            this.selectTodos();
        }

    }

    selectTodos(filterParameter?, filterValue?)
    {
        this.selectedTodos = [];

        // If there is no filter, select all todos
        if ( filterParameter === undefined || filterValue === undefined )
        {
            this.selectedTodos = this.todos;
        }
        else
        {
            this.selectedTodos.push(...
                this.todos.filter(todo => {
                    return todo[filterParameter] === filterValue;
                })
            );
        }

        // Trigger the next event
        this.onSelectedTodosChanged.next(this.selectedTodos);
    }

    deselectTodos()
    {
        this.selectedTodos = [];

        // Trigger the next event
        this.onSelectedTodosChanged.next(this.selectedTodos);
    }

    /**
     * Set current todo by id
     * @param id
     */
    setCurrentTodo(id)
    {
        this.currentTodo = this.todos.find(todo => {
            return todo.id === id;
        });
        this.onCurrentTodoChanged.next([this.currentTodo, 'edit']);

        const tagHandle    = this.routeParams.tagHandle,
              filterHandle = this.routeParams.filterHandle,
              selectedDate = this.routeParams.selectedDate;

        if ( tagHandle )
        {
            this.location.go('company/'+ this.routeParams.companyId +'/appointments/tag/' + tagHandle + '/' + id);
        }
        else if ( filterHandle )
        {
            this.location.go('company/'+ this.routeParams.companyId +'/appointments/filter/' + filterHandle + '/' + id);
        }
        else if ( selectedDate )
        {
            this.location.go('company/'+ this.routeParams.companyId +'/appointments/date/' + selectedDate + '/' + id);
        }
        else
        {
            this.location.go('company/'+ this.routeParams.companyId +'/appointments/' + id);
        }
    }

    /**
     * Toggle tag on selected todos
     * @param tagId
     */
    toggleTagOnSelectedTodos(tagId)
    {
        this.selectedTodos.map(todo => {
            this.toggleTagOnTodo(tagId, todo);
        });
    }

    toggleTagOnTodo(tagId, todo)
    {
        const index = todo.tags.indexOf(tagId);

        if ( index !== -1 )
        {
            todo.tags.splice(index, 1);
        }
        else
        {
            todo.tags.push(tagId);
        }

        this.saveTodo(todo);
    }

    hasTag(tagId, todo)
    {
        if ( !todo.tags )
        {
            return false;
        }

        return todo.tags.indexOf(tagId) !== -1;
    }

    /**
     * Update the todo
     * @param todo
     * @returns {Promise<any>}
     */
    saveTodo(todo)
    {
        if(todo.id !== 0)
        {
            return new Promise((resolve, reject) => {

                this.http.put(environment.API_URL + "/appointments/"+ todo.id, {...todo}, this.httpOptions)
                    .subscribe(() => {
    
                        this.getTodos().then(todos => {
    
                            resolve(todos);
    
                        }, reject);
                    });
            });
        }
        else
        {
            return new Promise((resolve, reject) => {

            this.http.post(environment.API_URL + "/appointments/", {...todo}, this.httpOptions)
                .subscribe(() => {

                    this.getTodos().then(todos => {

                        resolve(todos);

                    }, reject);
                });
        });
        }        
    }

    /**
     * Delete the todo
     * @param todo
     * @returns {Promise<any>}
     */
    deleteTodo(todo)
    {
        console.log("deleting an appointment");
        return new Promise((resolve, reject) => {

            this.http.delete(environment.API_URL +"/appointments/"+ todo.id, this.httpOptions)
                .subscribe(response => {

                    this.getTodos().then(todos => {
                        todos.find(x=> x.id === todo.id)[0];
                        resolve(todos);

                    }, reject);
                });
        });
    }
}
