import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from '../../_services/company.service';
import { LocationModel } from '../../_models/associate/locations.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  companyId: number;
  boards: LocationModel[];

  constructor(private companyService: CompanyService, private router: Router,
  private activatedRoute: ActivatedRoute) {
   }

  ngOnInit() {
    this.activatedRoute.params.subscribe(result=> {
      this.companyId = result.companyId;
    })

    this.boards = this.companyService.getLocations(this.companyId);
    if(this.boards.length == 1)
    {
      this.router.navigateByUrl("company/locations/"+this.boards[0].locationId);
    }
  }
}
