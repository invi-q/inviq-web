import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../../_services/auth.service';
import { CompanyModel } from '../../_models/associate/company.model';
import { fuseAnimations } from '@fuse/animations';
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
  animations : fuseAnimations
})

export class LocationComponent implements OnInit {
  boards: CompanyModel[]
  constructor(private authService: AuthService, private router: Router) {
   }

  ngOnInit() {
    this.authService.getProfile().subscribe(result=>{
      this.boards = result.associatedCompanies
    });
  }
}
