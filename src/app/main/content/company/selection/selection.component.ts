import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../../_services/auth.service';
import { CompanyModel } from '../../_models/associate/company.model';
import { fuseAnimations } from '@fuse/animations';
@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss'],
  animations : fuseAnimations
})

export class SelectionComponent implements OnInit {
  boards: CompanyModel[]
  constructor(private authService: AuthService, private router: Router) {
   }

  ngOnInit() {
    this.authService.getProfile().subscribe(result=>{
      this.boards = result.associatedCompanies
      if(this.boards.length == 1)
      {
        this.router.navigateByUrl("company/"+this.boards[0].companyId + "/appointments");
      }
    });
  }
}
