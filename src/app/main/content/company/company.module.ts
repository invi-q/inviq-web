import { NgModule } from '@angular/core';
import { UiComponentModule } from '../shared/ui-component/ui-component.module';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LocationComponent } from './location/location.component';
import { SelectionComponent } from './selection/selection.component';

const routes = [
  {
      path        : 'locations',
      component: LocationComponent,
      children: [{
        path: ':locationId',
        component: LocationComponent
      }]
  },
  {
      path        : ':companyId/appointments',
      loadChildren: './todo/todo.module#FuseTodoModule'
  },
  {
    path: 'home/:companyId',
    component: HomeComponent
  },
  {
    path        : 'selection',
    component: SelectionComponent
},
{
  path: '**',
  redirectTo: 'selection' 
}
];

@NgModule({
  imports: [
    UiComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeComponent, LocationComponent, SelectionComponent ]
})
export class CompanyModule { }
