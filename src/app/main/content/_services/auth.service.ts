import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as moment from "moment";
import { environment } from "environments/environment";
import { catchError, map, tap } from "rxjs/operators";
import { TokenDto } from "../_models/auth/token.model";
import { LoginModel } from "../_models/auth/login.model";
import { RegisterModel } from "../_models/auth/register.model";
import { ProfileModel } from "../_models/associate/profile.model";

@Injectable({
  providedIn: "root"
})

export class AuthService {
   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  public login(loginModel: LoginModel) {
    
    return this.http.post(environment.API_URL + "/associates/login", loginModel, this.httpOptions)
    .pipe(
      tap((result: TokenDto) => {
        this.setToken(result.access_token);
        this.setExpires(result.expires_in);
      })
    );
  }

  public getProfile() {
    if(this.isAuthenticated())
    {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          "Authorization": "Bearer "+ this.getToken()
        })
      };
    return this.http.get(environment.API_URL + "/associates/profile", this.httpOptions)
    .pipe(
      tap((result:ProfileModel)=>{
        this.setProfileInfo(result);
      })
    );
  }
}

private setProfileInfo(profileModel: ProfileModel){
  localStorage.setItem("full_name", profileModel.firstName + " " + profileModel.lastName );
  localStorage.setItem("companies_mapped_to", JSON.stringify(profileModel.associatedCompanies));
}
  private setToken(token: string): void {
    localStorage.setItem("access_token", token);
  }

  public getToken(): string {
    return localStorage.getItem("access_token");
  }

  private removeToken(): void {
    localStorage.removeItem("access_token");
  }

  private setExpires(expires): void {
    const now = moment();
    const expireAt = now.add("second", parseInt( expires, 0)).format("X");
    localStorage.setItem("expire_at", expireAt);
  }

  private getExpires(): string {
    return localStorage.getItem("expire_at");
  }

  public logout() {
    return this.http.post(environment.API_URL + "v1/auth/logout", {}).pipe(
      tap(result => {
        localStorage.removeItem("access_token");
        localStorage.removeItem("expire_at");
      })
    );
  }

  isTokenExpired() {
    const now = moment();
    const expireAt = moment.unix(parseInt(this.getExpires(), 0));
    return expireAt.isBefore(now);
  }

  isAuthenticated(): boolean {
    if (!this.isTokenExpired() && this.getToken() !== null && this.getExpires !== null) {
      return true;
    }
    return false;
  }

  public register(registerModel: RegisterModel, companyId: number){    
    console.log(registerModel);
    return this.http.post(environment.API_URL + "/associates/register/"+ companyId, registerModel, this.httpOptions)
    .pipe(
      tap((result: TokenDto) => {
        this.setToken(result.access_token);
        this.setExpires(result.expires_in);
      })
    );
  }

}
