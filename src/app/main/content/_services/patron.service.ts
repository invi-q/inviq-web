import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "environments/environment";
import { catchError, map, tap } from "rxjs/operators";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";
import { PatronModel } from "../_models/patron/patron.model";

@Injectable({
  providedIn: 'root'
})
export class PatronService {
  httpOptions: any;
    constructor(private http: HttpClient,
    private authService: AuthService) { }

    public getPatrons() {  
    if(this.authService.isAuthenticated())
    {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          "Authorization": "Bearer "+ this.authService.getToken()
        })
      };
    return this.http.get(environment.API_URL + "/patrons", this.httpOptions)
    .pipe(
    );
  }
    }
}
