import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "environments/environment";
import { catchError, map, tap } from "rxjs/operators";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";
import { ScheduleModel } from "../_models/associate/schedule.model";
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService  {
  httpOptions: any;
    constructor(private http: HttpClient,
    private authService: AuthService) { }

    public getSchedules(locationId: number, associateId: number) : Observable<any> {  
    if(this.authService.isAuthenticated())
    {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          "Authorization": "Bearer "+ this.authService.getToken()
        })
      };
    return this.http.get(environment.API_URL + "/schedules/location/"+locationId+"/associate/"+associateId, this.httpOptions)
    .pipe(
      tap(result => console.log(result))
    );
  }
    }
}
