import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "environments/environment";
import { catchError, map, tap } from "rxjs/operators";
import { AuthService } from "./auth.service";
import { LocationModel } from "../_models/associate/locations.model";
import { Observable } from "rxjs";
import { CompanyModel } from "../_models/associate/company.model";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
httpOptions: any;
  constructor(private http: HttpClient,
  private authService: AuthService) { }

  public getLocations(companyId: number): LocationModel[]{
    if(this.authService.isAuthenticated())
    {
      return this.getCompany(companyId).locations
    }
  }

  
  public getCompany(companyId:number): CompanyModel {
    let selectedCompany: CompanyModel;
    if(this.authService.isAuthenticated()){ 
      const companies = <CompanyModel[]>JSON.parse(localStorage.getItem("companies_mapped_to"));
      companies.forEach(company => {
        if(company.companyId == companyId){
          selectedCompany = company;
        }
      });
      return selectedCompany;
    }
  }

  
  public getAssociateName(){
    if(this.authService.isAuthenticated()){    
      localStorage.getItem("full_name");
    }
  }
}
