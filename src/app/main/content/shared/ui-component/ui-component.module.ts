import { NgModule } from '@angular/core';
import { FuseAngularMaterialModule } from '../../components/angular-material/angular-material.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, 
  MatChipsModule, MatDatepickerModule, MatDialogModule, MatIconModule, MatListModule, 
  MatMenuModule, MatProgressBarModule, MatRippleModule, MatSidenavModule, MatToolbarModule, MatTooltipModule } from '@angular/material';


@NgModule({
  imports: [
    FuseSharedModule,
    FuseAngularMaterialModule,

    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule, 
    MatChipsModule, MatDatepickerModule, MatDialogModule, MatIconModule, MatListModule, 
    MatMenuModule, MatProgressBarModule, MatRippleModule, MatSidenavModule, MatToolbarModule, MatTooltipModule
  ],
  exports:[
    FuseSharedModule,
    FuseAngularMaterialModule,

    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule, 
    MatChipsModule, MatDatepickerModule, MatDialogModule, MatIconModule, MatListModule, 
    MatMenuModule, MatProgressBarModule, MatRippleModule, MatSidenavModule, MatToolbarModule, MatTooltipModule
  ],
  declarations: []
})
export class UiComponentModule { }
