import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseAngularMaterialModule } from '../components/angular-material/angular-material.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { LoginModule } from './login/login.module';
import { RegisterModule } from './register/register.module';
import { ResetPasswordModule } from './reset-password/reset-password.module';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { ScheduleComponent } from './schedule/schedule.component';

const routes = [
  {
      path        : 'login',
      loadChildren: './login/login.module#LoginModule'
  },
  {
      path        : 'register',
      loadChildren: './register/register.module#RegisterModule'
  },
  {
      path        : 'forgotpassword',
      loadChildren: './forgot-password/forgot-password.module#ForgotPasswordModule'
  },
  {
      path        : 'reset-password',
      loadChildren: './reset-password/reset-password.module#ResetPasswordModule'
  },
  {
      path        : '**',
      loadChildren: './login/login.module#LoginModule'
  }
];

@NgModule({
  imports: [
    LoginModule,
    RegisterModule,
    ForgotPasswordModule,
    ResetPasswordModule,

    FuseSharedModule,
    RouterModule.forChild(routes),
    FuseAngularMaterialModule,

    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
  ]
})
export class AssociateModule { }
