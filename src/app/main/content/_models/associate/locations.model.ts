import { AssociateModel } from "./associate.model";

export class LocationModel {
    public locationId: number
    public locationName: string
    public companyId: number
    public associates: AssociateModel[]
}
