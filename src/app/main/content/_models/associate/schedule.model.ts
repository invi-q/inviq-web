export class ScheduleModel {
    public scheduleId : number;
    public startTime: string;
    public endTime : string;
    public dayOfWeek : number;
}
