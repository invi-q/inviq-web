import { LocationModel } from "./locations.model";

export class CompanyModel {
    public companyId: number
    public companyName: string
    public userRole: string
    public userRoleId: number
    public locations: LocationModel[]
}
