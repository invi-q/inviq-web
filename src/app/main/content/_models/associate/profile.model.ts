import { CompanyModel } from "./company.model";

export class ProfileModel {
    public id: number
    public firstName: string
    public lastName: string
    public associatedCompanies: CompanyModel[]
}
