export class PatronModel {
    public patronName: string;
    public patronId: number;
    public phoneNumber: string;
    public email: string;
}
