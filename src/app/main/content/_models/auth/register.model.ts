export class RegisterModel{
    public firstName: string;
    public lastName: string;
    public email: string;
    public primaryPhone: string;
    public password: string;
    public confirmPassword: string;
}