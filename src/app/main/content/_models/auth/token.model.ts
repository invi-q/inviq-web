export class TokenDto {
    public access_token: string;
    public expires_in: number;
    public token_type: string;
    public refresh_token: string;
}
